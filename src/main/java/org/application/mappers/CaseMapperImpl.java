package org.application.mappers;

import java.util.Date;

import org.application.dto.CaseReponseDTO;
import org.application.dto.CaseRequestDTO;
import org.application.model.Case;
/*
 * Implementation de l'interface mapping
 * A la place du plug-in  mapstruts
 */
public class CaseMapperImpl implements CaseMappers{
/*
 * Transformation de RequestDTO en l'objet Cas
 */
	@Override
	public Case casRequestDtoToCas(CaseRequestDTO casRequestDTO) {
		Case cas = new Case();
		cas.setIdCase(casRequestDTO.getIdCas());
		cas.setDateCreation(new Date());
		cas.setDateDernieMaj(new Date());
		cas.setDescription(casRequestDTO.getDescription());
		cas.setTitre(casRequestDTO.getTitre());
		return cas;
	}
/*
 * Transformation de l'objet Cas en Reponse DTO 
 */
	@Override
	public CaseReponseDTO casToCasReponseDTO(Case cas) {
		CaseReponseDTO casReponseDTO = new CaseReponseDTO();
		casReponseDTO.setIdCas(cas.getIdCase());
		casReponseDTO.setDateCreation(cas.getDateCreation());
		casReponseDTO.setDateDernieMaj(cas.getDateDernieMaj());
		casReponseDTO.setDescription(cas.getDescription());
		casReponseDTO.setTitre(cas.getTitre());
		return casReponseDTO;
	}

}
