package org.application.mappers;

import org.application.dto.CaseReponseDTO;
import org.application.dto.CaseRequestDTO;
import org.application.model.Case;
/*
 * Mapping objet-objet
 * Coté requete client vers l'objet
 */
public interface CaseMappers {
	public Case casRequestDtoToCas(CaseRequestDTO casRequestDTO);
	public CaseReponseDTO casToCasReponseDTO (Case cas);
	
	

}
