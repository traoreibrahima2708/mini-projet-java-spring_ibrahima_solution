package org.application.dto;

import java.math.BigInteger;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class CaseRequestDTO {
	private BigInteger idCas;
	private Date dateCreation;
	private Date dateDernieMaj ;
	private String titre;
	private String description;
	
}
