package org.application.init;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.application.dto.CaseRequestDTO;
import org.application.service.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InitCaseServiceImp implements InitCaseService {
	@Autowired
	CaseService casService;
	@Override
	public void initCreateCas() {
		final int sizeCase = 10;
		int index=0;
				for(int i=0; i<=sizeCase; i++) {
					CaseRequestDTO casRequestDTO=new CaseRequestDTO();
					index++;
					casRequestDTO.setIdCas(BigInteger.valueOf(index));
					casRequestDTO.setDateCreation(new Date());
					casRequestDTO.setDateDernieMaj(new Date());
					casRequestDTO.setTitre("titre " +index);
					casRequestDTO.setDescription("description " +index);
					casService.saveCas(casRequestDTO);
					
				}	
	}

	@Override
	public void initUpdateCas() {
		//update
				casService.updateCas(new BigInteger("2"), new CaseRequestDTO(new BigInteger("2"),new Date(), new Date(), "Titre_Test_Update2", "Descriptio_Test_upate2"));
				casService.updateCas(new BigInteger("3"), new CaseRequestDTO(new BigInteger("3"),new Date(), new Date(), "Titre_Test_update3", "Descriptio_Test_update3"));	
	}

	@Override
	public void initDeleteCas() {
		casService.deleteCas(new BigInteger("9"));
		
	}

	@Override
	public void initListeCas() {
		// TODO Auto-generated method stub
		
	}

}
