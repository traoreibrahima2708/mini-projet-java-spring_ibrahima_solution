package org.application.init;
/*
 * Service d'initialisation de l'application
 * pour creer des objets CAS
 */
public interface InitCaseService {
	
	public void initCreateCas();
	public void initUpdateCas();
	public void initDeleteCas();
	public void initListeCas();

}
