package org.application.service;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.application.dao.CaseRepository;
import org.application.dto.CaseReponseDTO;
import org.application.dto.CaseRequestDTO;
import org.application.mappers.CaseMapperImpl;
import org.application.mappers.CaseMappers;
import org.application.model.Case;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;
/*
 * Implementation de service CRUD CAS
 * en utilisant Rest template pour asynchroniser les requetes
 */
@Service
@Transactional
public class CaseServiceImpl implements CaseService{
	@Autowired
	private CaseRepository casRepository;
	private CaseMappers casMappers ;
	
public CaseServiceImpl() {
		super();
		casMappers = new CaseMapperImpl();
	}

	@Override
	public ResponseEntity<CaseReponseDTO> saveCas(CaseRequestDTO casRequestDTO) {
		Case cas = casMappers.casRequestDtoToCas(casRequestDTO);
		CaseReponseDTO casReponseDTO = casMappers.casToCasReponseDTO(casRepository.save(cas));
		return ResponseEntity.ok(casReponseDTO);
	}

	@Override
	public ResponseEntity<CaseReponseDTO> updateCas(BigInteger idCas, CaseRequestDTO casRequestDTO) {
		Case cas = casRepository.findById(idCas)
				.orElseThrow(()-> new ResourceAccessException("Cas not found"+idCas));
		cas.setDateCreation(cas.getDateCreation());
		cas.setDateDernieMaj(new Date());
		cas.setDescription(casRequestDTO.getDescription());
		cas.setTitre(casRequestDTO.getTitre());
		CaseReponseDTO casReponseDTO = casMappers.casToCasReponseDTO(casRepository.save(cas));
		return ResponseEntity.ok(casReponseDTO);
		
	}

	@Override
	public ResponseEntity<CaseReponseDTO> getCas(BigInteger idCas) {
		Case cas = casRepository.findById(idCas)
				.orElseThrow(()->new ResourceAccessException("Cas not found" +idCas));
		CaseReponseDTO casReponseDTO = casMappers.casToCasReponseDTO(cas);
		return ResponseEntity.ok(casReponseDTO);
	}

	@Override
	public ResponseEntity<List<CaseReponseDTO>> listCas() {
		List<CaseReponseDTO> listCasReponseDTO = casRepository.findAll()
				.stream()
				.map(cas->casMappers.casToCasReponseDTO(cas))
				.toList();
		return ResponseEntity.ok(listCasReponseDTO);
	}

	@Override
	public ResponseEntity<Map<String, Boolean>> deleteCas(BigInteger idCas) {
		Case cas = casRepository.findById(idCas)
				.orElseThrow(()->new ResourceAccessException("Cas not found" +idCas));
		casRepository.delete(cas);
		HashMap<String, Boolean> deleteCasReponse = new HashMap<>();
		deleteCasReponse.put("Cas supprime", true);
		return ResponseEntity.ok(deleteCasReponse);
	}

}
