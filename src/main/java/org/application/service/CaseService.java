package org.application.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.application.dto.CaseReponseDTO;
import org.application.dto.CaseRequestDTO;
import org.springframework.http.ResponseEntity;
/*
 * Service CRUD CAS
 */
public interface CaseService {
	public ResponseEntity<CaseReponseDTO> saveCas(CaseRequestDTO casRequestDTO);
	public ResponseEntity<CaseReponseDTO> updateCas(BigInteger idCas, CaseRequestDTO casRequestDTO);
	public ResponseEntity<CaseReponseDTO> getCas(BigInteger idCas);
	public ResponseEntity<List<CaseReponseDTO>> listCas();
	public ResponseEntity<Map<String, Boolean>> deleteCas(BigInteger idCas);
	
}
