package org.application.web;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.application.dto.CaseReponseDTO;
import org.application.dto.CaseRequestDTO;
import org.application.service.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/*
 * Controller Case
 */
@RestController
@RequestMapping(path="/")
@CrossOrigin("*")
public class CaseRestController {
	
	@Autowired
	private CaseService casService;

	@PostMapping(path="cases/")
	public ResponseEntity<CaseReponseDTO> saveCas(@RequestBody CaseRequestDTO casRequestDTO) {
		
		return casService.saveCas(casRequestDTO);
	}

	@PutMapping(path="cases/{idCas}")
	public ResponseEntity<CaseReponseDTO> updateCas(@PathVariable(name="idCas") BigInteger idCas, @RequestBody CaseRequestDTO casRequestDTO) {
		
		return casService.updateCas(idCas, casRequestDTO);
		
	}

	@GetMapping(path="cases/{idCas}")
	public ResponseEntity<CaseReponseDTO> getCas(@PathVariable(name="idCas") BigInteger idCas) {
		
		return casService.getCas(idCas);
	}

	@GetMapping(path="cases/")
	public ResponseEntity<List<CaseReponseDTO>> listCas() {
		
		return casService.listCas();
	}

	@DeleteMapping(path="cases/{idCas}")
	public ResponseEntity<Map<String, Boolean>> deleteCas(@PathVariable(name="idCas") BigInteger idCas) {
		
		return casService.deleteCas(idCas);
	}
}
