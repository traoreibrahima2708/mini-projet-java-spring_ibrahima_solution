package org.application;

import org.application.init.InitCaseService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ApplicationDreamCase {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationDreamCase.class, args);
	}
	@Bean
	CommandLineRunner start(InitCaseService initCas) {
		return args->{
			initCas.initCreateCas();
			initCas.initUpdateCas();
			initCas.initDeleteCas();
			initCas.initListeCas();
			
		};
}
}
