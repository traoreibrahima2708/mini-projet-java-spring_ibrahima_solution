package org.application.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "case")
@Data @AllArgsConstructor @NoArgsConstructor
public class Case {
	@Id 
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	private BigInteger idCase;
	@Column(name = "Date_Creation")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreation;
	@Column(name = "Date_DernierMaj")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateDernieMaj ;
	@Column(name = "Titre", length=255)
	private String titre;
	@Column(name = "Description", length=2056)
	private String description;
	

}
