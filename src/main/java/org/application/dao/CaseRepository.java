package org.application.dao;

import java.math.BigInteger;

import org.application.model.Case;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CaseRepository extends JpaRepository<Case, BigInteger>{

}

